/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitCommitment;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author Emma
 */
public class UnitCommitmentModel {
   
    
    /*In this class I create an IP model for the original problem without Benders decomposition,
    so that I know the true solution to the problem and can compare it to the Bender-solution. */ 
    
    //I create an IloCplex object.
    private final IloCplex model;
    
    //I create all of the decition variables. 
    private final IloNumVar c[][];
    private final IloIntVar u[][];
    private final IloNumVar l[];
    private final IloNumVar p[][];
    
    //And an instance of the unit commitment problem
    private final UnitCommitmentProblem ucp;
    
    
    public UnitCommitmentModel(UnitCommitmentProblem ucp) throws IloException{
        
        //I set the unit commitment problem
        this.ucp = ucp;
        
        //I give the model an IloCplex object
        this.model = new IloCplex();
        
        //I assign the decition variables dimensions
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        u = new IloIntVar[ucp.getnGenerators()][ucp.getnPeriods()];
        l = new IloNumVar[ucp.getnPeriods()];
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        
        //I i assign each entrance in the arrays an object
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                c[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
                u[g-1][t-1] = model.boolVar();
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        for(int t=1;t<=ucp.getnPeriods();t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY);
        }
        
        
        //Create an object for the objective function
        IloLinearNumExpr obj = model.linearNumExpr();
        
        //The objective function is all the costs summed 
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                obj.addTerm(c[g-1][t-1], 1);
                obj.addTerm(u[g-1][t-1], ucp.getCommitmentCost(g));
            }
        }
        for(int t=1;t<=ucp.getnPeriods();t++){
            obj.addTerm(ucp.getLoadSheddingCost(t), l[t-1]);
            for(int g=1;g<=ucp.getnGenerators();g++){
                obj.addTerm(ucp.getMarginalCost(g), p[g-1][t-1]);
            }
        }
        
        //I tell the model to minimize the objective funtion
        model.addMinimize(obj);
        
        
        //Startup cost constraints (1b)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(u[g-1][t-1], ucp.getStartupCost(g));
                //I only subtract u_g,t-1 if t>1, since u takes the value 0 at t=0.
                if(t > 1){
                    rhs.addTerm(u[g-1][t-2], -ucp.getStartupCost(g));
                }
                model.addGe(c[g-1][t-1], rhs);
            }
        }
        
        
        //Constraints for the on-time for generators (1c)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                //For each gernerator and time period I calculate the minimum on time in that period for the generator
                double T = Math.min(t+ucp.getMinOnTime(g)-1, ucp.getnPeriods()); 
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int k=t;k<=T;k++){
                    lhs.addTerm(u[g-1][k-1], 1);
                    lhs.addTerm(u[g-1][t-1], -1);
                    //Again I only subtract u_g,t-1 if t>1.
                    if(t > 1){
                        lhs.addTerm(u[g-1][t-2], 1);
                    }
                }
                model.addGe(lhs, 0);
            }
        }
        
        //Constraints for the off-time for generatiors (1d)        
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                double T = Math.min(t+ucp.getMinOffTime(g)-1, ucp.getnPeriods());
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int k=t;k<=T;k++){
                    lhs.addTerm(u[g-1][k-1],-1);
                    lhs.addTerm(u[g-1][t-1], 1);
                    if(t > 1){
                        lhs.addTerm(u[g-1][t-2],-1);
                    }
                }
                model.addGe(lhs,t-1-T);
            }
        }
                    
        //Demand constraints (1e)
        for(int t=1;t<=ucp.getnPeriods();t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g=1;g<=ucp.getnGenerators();g++){
                lhs.addTerm(p[g-1][t-1], 1); 
            }
            lhs.addTerm(l[t-1], 1);
            model.addEq(lhs,ucp.getDemand(t));
        }
    
        //Lower bounds on power outputs for the (1f)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(u[g-1][t-1], ucp.getMinOutput(g));
                model.addGe(p[g-1][t-1], rhs);
            }
        }
    
        //Upper bounds on power outputs for the (1g)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(u[g-1][t-1], ucp.getMaxOutput(g));
                model.addLe(p[g-1][t-1], rhs);
            }
        }
    
        //Ramp-up conditions (1h)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(p[g-1][t-1], 1);
                if(t>1){
                    lhs.addTerm(p[g-1][t-2],-1);
                }
                model.addLe(lhs, ucp.getRampUpLim(g));
            }
        }
        
        //Ramp-down conditions (1i)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t>1){
                    lhs.addTerm(p[g-1][t-2],1);
                }
                lhs.addTerm(p[g-1][t-1], -1);
                model.addLe(lhs, ucp.getRampDownLim(g));
            }
        }    
        
    
    
    }
    
    //I solve the model and print the optimal value.
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
        //Print the optimal solution
        System.out.println("Optimal objective value "+model.getObjValue());
        /*
        System.out.println("Optimal solution ");
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                System.out.println("u_"+g+","+t+" = "+model.getValue(u[g-1][t-1]));
            }
        }
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                System.out.println("p_"+g+","+t+" = "+model.getValue(p[g-1][t-1]));
            }
        }
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                System.out.println("c_"+g+","+t+" = "+model.getValue(c[g-1][t-1]));
            }
        }
        for(int t=1;t<=ucp.getnPeriods();t++){
            System.out.println("l_"+t+" = "+model.getValue(l[t-1]));
        }
        */
   
    }
    
    //Release all elements
    public void end(){
        model.end();
    }
    
    
    
    
}