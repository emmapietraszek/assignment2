/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitCommitment;
/**
 *
 * @author Emma
 */
public class UnitCommitmentProblem {

    /*In this class I create a constructor for the unit commitment problem.
    I also create getters for all the different parameters, which I will use in the other classes. */

    //The umit commitment problem has a set for time periods and generators    
    private final int nPeriods;
    private final int nGenerators;
    
    //There I have 10 different cind of parameters that is used in the problem
    private final String generatorNames[];
    private final double commitmentCosts[];
    private final double marginalCosts[];
    private final double startupCosts[];
    private final double loadSheddingCosts[];
    private final double minOnTimes[];
    private final double minOffTimes[];
    private final double minOutputs[];
    private final double maxOutputs[];
    private final double rampUpLims[];
    private final double rampDownLims[];
    private final double demands[];
    
    //This is a constructor that creates a instance of the unit commitment problem
    public UnitCommitmentProblem(String generatorNames[],int nPeriods,int nGenerators,double commitmentCosts[],double marginalCosts[],double startupCosts[],double loadSheddingCosts[], 
            double minOnTimes[],double minOffTimes[],double minOutputs[],double maxOutputs[],double rampUpLims[],double rampDownLims[],double demands[]){
        
        this.generatorNames = generatorNames;
        this.nPeriods = nPeriods;
        this.nGenerators = nGenerators;
        this.commitmentCosts = commitmentCosts;
        this.marginalCosts = marginalCosts;
        this.startupCosts = startupCosts;
        this.loadSheddingCosts = loadSheddingCosts;
        this.minOnTimes = minOnTimes;
        this.minOffTimes = minOffTimes;
        this.minOutputs = minOutputs;
        this.maxOutputs = maxOutputs;
        this.rampUpLims = rampUpLims;
        this.rampDownLims = rampDownLims;
        this.demands = demands;    
    }
    

    //Get the name of a generator
    public String getGeneratorName(int g){
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        } 
        return generatorNames[g-1];
    }
    
    
    //Get the number of time periods in the problem
    public int getnPeriods() {
        return nPeriods;
    }
    
    //Get the number of generators in the problem
    public int getnGenerators() {
        return nGenerators;
    }
  
    //Get the commitment cost for generator number g
    public double getCommitmentCost(int g){
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return commitmentCosts[g-1];
    }
   
    //Get the marginal cost for generator number g
    public double getMarginalCost(int g){
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return marginalCosts[g-1];
    }
    
    //Get the startup cost for generator number g
    public double getStartupCost(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return startupCosts[g-1];
    }

    //Get the load shedding cost for time period t
    public double getLoadSheddingCost(int t) {
        if(t < 1 || t > nPeriods){
            throw new IllegalArgumentException("The times period must be in [1,"+nPeriods+"].");
        }
        return loadSheddingCosts[t-1];
    }
    
    //Get the minimum time generator number g nust be on
    public double getMinOnTime(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOnTimes[g-1];
    }

    //Get the minimum time generator number g must be off
    public double getMinOffTime(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOffTimes[g-1];
    }

    //Get the minimum power output of generator number g
    public double getMinOutput(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return minOutputs[g-1];
    }

    //Get the maximum power output of generator number g
    public double getMaxOutput(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return maxOutputs[g-1];
    }

    //Get the ramp-up limit for generator number g
    public double getRampUpLim(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return rampUpLims[g-1];
    }

    //Get the ramp-down limit for generator number g
    public double getRampDownLim(int g) {
        if(g < 1 || g > nGenerators){
            throw new IllegalArgumentException("The generator number must be in [1,"+nGenerators+"].");
        }
        return rampDownLims[g-1];
    }

    //Get the demand in time period t 
    public double getDemand(int t) {
        if(t < 1 || t > nGenerators){
            throw new IllegalArgumentException("The time period must be in [1,"+nPeriods+"].");
        }
        return demands[t-1];
    }
    

            
    
}
