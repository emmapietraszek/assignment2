/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitCommitment;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;



/**
 *
 * @author Emma
 */
public class MasterProblem {
    
    /* In this class I create the master problem and define the method Callback, that tells Cplex how to solve the master problem.
    In the unit commitment problem the u-variables tells wether a generator is turned on or off in a sertan time period. So u is a bnary variable,
    which complicates the problem. Therefor I choose to have the u-varibales in my master problem. In the structure of the problem I notice that 
    the c-variables are only appearing in the constraint (1b) together with the u-varibales. Therefore I choose to also have the c-varibales in my master problem.
    The c-variables will then not need to be passed to the sub problems since they only appear in the master problem.
    By chooing these variables I will have constraints (1b), (1c) and (1d) in my master problem.
    */
    
    //I create an IloCplex object for the model
    private final IloCplex model;
    
    //I create the decition variables. 
    private final IloNumVar c[][];
    private final IloIntVar u[][];
    
    //I also need a phi
    private final IloNumVar phi;
    
    //And an instance of the unit commitment problem
    private final UnitCommitmentProblem ucp;
    
    
    //I now create the master problem, which I can do from only a instance of the unit commitment problem.
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException{
        
        //I set the unit commitment problem
        this.ucp = ucp;
        
        //I give the model a IloCplex object
        model = new IloCplex();
        
        //I give the decition variables a dimension
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        u = new IloIntVar[ucp.getnGenerators()][ucp.getnPeriods()];
        //I do not define u at time 0, but instead make an "if"-statement in the individual constraints.
        
        //Now I assign each position in c and u an IloNumVar object or a binary variable.
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                c[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"c_"+g+","+t);
                u[g-1][t-1] = model.boolVar(); //u is binary
            }
        }
        
        //I also assign a object to phi
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        //I create an object for the objective function
        IloLinearNumExpr obj = model.linearNumExpr();
        
        //I now tell the model what the objective function is the total startup costs and commitment costs plus phi
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                obj.addTerm(c[g-1][t-1], 1);
                obj.addTerm(u[g-1][t-1], ucp.getCommitmentCost(g));
            }
        }
        obj.addTerm(phi, 1);
        
        //I tell the model to minimize the objective funtion
        model.addMinimize(obj);
        
        
        
        //Startup cost constraints (1b)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(u[g-1][t-1], ucp.getStartupCost(g));
                //I only subtract u_g,t-1 if t>1, since u_g,t takes the value 0 at t=0.
                if(t > 1){
                    rhs.addTerm(u[g-1][t-2], -ucp.getStartupCost(g));
                }
                model.addGe(c[g-1][t-1], rhs);
            }
        }
        
        
        //Constraints for the on-time for generators (1c)
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                //For each gernerator and time period I calculate the minimum on time in that period for the generator.
                double T = Math.min(t+ucp.getMinOnTime(g)-1, ucp.getnPeriods()); 
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int k=t;k<=T;k++){ //I sum from t
                    lhs.addTerm(u[g-1][k-1], 1);
                    lhs.addTerm(u[g-1][t-1], -1);
                    //Again I only subtract u_g,t-1 if t>1.
                    if(t > 1){
                        lhs.addTerm(u[g-1][t-2], 1);
                    }
                }
                model.addGe(lhs, 0);
            }
        }
        
        //Constraints for the off-time for generatiors (1d)        
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                //For each ganerator and time period I calculate the minimum on time in that period for the generator.
                double T = Math.min(t+ucp.getMinOffTime(g)-1, ucp.getnPeriods());
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int k=t;k<=T;k++){
                    lhs.addTerm(u[g-1][k-1],-1);
                    lhs.addTerm(u[g-1][t-1], 1);
                    if(t > 1){
                        lhs.addTerm(u[g-1][t-2],-1);
                    }
                }
                //The sum over 1 from t to T must be T-t+1, so I can add the inequality as below
                model.addGe(lhs,t-1-T);
            }
        }
     
        
    }


    //Solve the problem with Benders-decomposition
    public void solve() throws IloException{
        //I tell Cplex that I want to use the method Callback that I define below
        model.use(new Callback());
        //Limit the printed information
        model.setOut(null);  
        //Solve the problem
        model.solve();
    }
    
    
    //I base my method on the LazyConstraintCallback, so that I can check feasibility
    //and optimality every time the branch and bound method reaches an integer node.
    private class Callback extends IloCplex.LazyConstraintCallback{
    
        public Callback(){
        }
        
        //Here I create the main method for the Callback class
        @Override
        protected void main() throws IloException{
            //I start by getting the solution from the current node
            double U[][] = getU();
            double C[][] = getC();
            double Phi = getPhi();
            
            /*Now I want to check if the solution to the master problem is second stage feasible.
            First I create and solve the feasibility sub problem */
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(ucp,U);
            fsp.solve();
            //I create a double that contains the optimal objective in the feasibility sub problem.
            double fspObjective = fsp.getObjective();
            //I print the value
            System.out.println("FSP objective: "+fsp.getObjective());
            
            //The next step is to check if the solution is second stage feasible.
            if(fsp.getObjective()>=0+1e-9){
                
                /*If the optimal objective in the feasibility sub problem is positive the solution
                is not second stage feasible, so I generate a feasibility cut for the master problem.
                This cut will cut off the current solution, but possibly also other non-feasible solutions.
                First I print that I am generating a feasibility cut*/
                System.out.println("Generating feasibility cut");
                
                //I get the constant and the linear term of the cut from the feasibility sub problem.
                double constant = fsp.getCutConstant();
                //I use u insted of U, since I create a linear term for the varible u in the master problem
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(u);
                
                //Add the feasibility cut to the master problem
                add(model.le(linearTerm,-constant));
                
            }else{
                /* If the optimal objective value of the feasibility sub problem is non-positive the solution
                to the master problem is second stage feasible. The next step is to check if the solution is 
                in fact optimal.
                I start by creating the optimality sub problem by passing the current solution U to the 
                optimality sub problem and solve it.*/
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                
                //I create a double for the optimal objective value from the optimality sub problem and print it together with phi.
                double ospObjective = osp.getObjective();
                System.out.println("Phi "+Phi);
                System.out.println("OSP objective "+ospObjective);
                
                //I now check the optimality condition
                if(Phi >= ospObjective -1e-9){
                    /*If the optimal phi from the master problem is greater than the optimal objective value 
                    in the optimality sub problem, the current solution is optimal.
                    I print the message that the current node is optimal.*/
                    System.out.println("Current node is optimal");
                    
                    /* I also print the optimal solution.
                    System.out.println("The Benders optimal solution is");
                    for(int g=1;g<=ucp.getnGenerators();g++){
                        for(int t=1;t<=ucp.getnPeriods();t++){
                            System.out.println("c_"+g+","+t+" = "+C[g-1][t-1]);
                        }
                    }
                    
                    for(int g=1;g<=ucp.getnGenerators();g++){
                        for(int t=1;t<=ucp.getnPeriods();t++){
                            System.out.println("u_"+g+","+t+" = "+U[g-1][t-1]);
                        }
                    }
                    double L[] = osp.getOptimalL();
                    for(int g=1;g<=ucp.getnGenerators();g++){
                        for(int t=1;t<=ucp.getnPeriods();t++){
                            System.out.println("l_"+g+","+t+" = "+L[t-1]);
                        }
                    }
                    double P[][] = osp.getOptimalP();
                    for(int g=1;g<=ucp.getnGenerators();g++){
                        for(int t=1;t<=ucp.getnPeriods();t++){
                            System.out.println("p_"+g+","+t+" = "+P[g-1][t-1]);
                        }
                    }
                    */

                }else{
                    /* If phi is not greater than the optimal objective of the optimality sub problem, the current 
                    solution is not optimal. I will therefore add a optimality cut, that cuts off the current
                    solution, and others that are not optimal.
                    First I tell the method to print that it is generating a optimality cut.*/
                    System.out.println("Gernerating optimality cut");
                    //Then I get the constant and linear terms from the optimality sub problem to generate my cut.
                    double constant = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    //I add phi to the cutTerm
                    cutTerm.addTerm(-1, phi);
                    //I add a cut to the master problem
                    add(model.ge(-constant,cutTerm));
                }

            }
 
        }

        /*I create getters for u, c and phi, since I will need the current solution of
        a node in my Callback method. */
    
        //Get the current optimal solution of u in a node
        public double[][] getU() throws IloException{
            double U[][] = new double[ucp.getnGenerators()][ucp.getnPeriods()];
            for(int g=1;g<=ucp.getnGenerators();g++){
                for(int t=1;t<=ucp.getnPeriods();t++){
                    U[g-1][t-1] = getValue(u[g-1][t-1]);
                }
            }
            return U;
        }    
    
        //Get the current optimal solution of c in a node
        public double[][] getC() throws IloException{
            double C[][] = new double[ucp.getnGenerators()][ucp.getnPeriods()];
            for(int g=1;g<=ucp.getnGenerators();g++){
                for(int t=1;t<=ucp.getnPeriods();t++){
                    C[g-1][t-1] = getValue(c[g-1][t-1]);
                }
            }
            return C;
        }
    
        //Get the current optimal solution of phi in a node
        public double getPhi() throws IloException{
            return getValue(phi);
        }
        
    }    
    

    /*I create a constructor that return the optimal objective of the problem
    solved with Benders decomposition.*/
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

   
    
    //Release all the elements
    public void end(){
        model.end();
    }
        
 
    
    
    
    
    
    
    
    
    
    
    
    
}
