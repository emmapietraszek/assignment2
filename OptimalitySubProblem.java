/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitCommitment;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 *
 * @author Emma
 */
public class OptimalitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final UnitCommitmentProblem ucp;
    private final IloRange demandConstraints[];
    private final IloRange LowOutputConstraints[][];
    private final IloRange HighOutputConstraints[][];
    private final IloRange RampUpConstraints[][];
    private final IloRange RampDownConstraints[][];
        
    public OptimalitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException{
    
        //I set the unit commitment problem.
        this.ucp = ucp;
        //I assign the model an IloCplex object.
        this.model = new IloCplex();
    
        //I tell the dimentions of my desition variables and assing each entrance an object
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        l = new IloNumVar[ucp.getnPeriods()];
    
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"p_"+g+"_"+t);
            }
        }
        for(int t=1;t<=ucp.getnPeriods();t++){
            l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"l_"+t);
        }
    
        //I create an object for the objective function
        IloLinearNumExpr obj = model.linearNumExpr();
    
        //The objective function is the load-shedding costs and the marginal costs
        for(int t=1;t<=ucp.getnPeriods();t++){
            obj.addTerm(ucp.getLoadSheddingCost(t), l[t-1]);
            for(int g=1;g<=ucp.getnGenerators();g++){
                obj.addTerm(ucp.getMarginalCost(g), p[g-1][t-1]);
            }
        }
        //I tell the model to minimize the objective function
        model.addMinimize(obj);
        
        //Demand constraints (1e)
        this.demandConstraints = new IloRange[ucp.getnPeriods()];
        for(int t=1;t<=ucp.getnPeriods();t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g=1;g<=ucp.getnGenerators();g++){
                lhs.addTerm(p[g-1][t-1], 1); 
            }
            lhs.addTerm(l[t-1], 1);
            demandConstraints[t-1] = model.addEq(lhs,ucp.getDemand(t));
        }
        
           
        //Lower bounds on power outputs for the (1f)
        this.LowOutputConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                LowOutputConstraints[g-1][t-1] = model.addGe(p[g-1][t-1], ucp.getMinOutput(g)*U[g-1][t-1]);
            }
        }
    
        //Upper bounds on power outputs for the (1g)
        this.HighOutputConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                HighOutputConstraints[g-1][t-1] = model.addLe(p[g-1][t-1], ucp.getMaxOutput(g)*U[g-1][t-1]);
            }
        }
    
        //Ramp-up conditions (1h)
        this.RampUpConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(p[g-1][t-1], 1);
                if(t > 1){
                    lhs.addTerm(p[g-1][t-2],-1);
                }
                RampUpConstraints[g-1][t-1] = model.addLe(lhs, ucp.getRampUpLim(g));
            }
        }
        
        //Ramp-down conditions (1i)
        this.RampDownConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(p[g-1][t-2],1);
                }
                lhs.addTerm(p[g-1][t-1], -1);
                RampDownConstraints[g-1][t-1] = model.addLe(lhs, ucp.getRampDownLim(g));
            }
        }    
        
 
    
    }

    //Solve the model    
    public void solve() throws IloException{
        //I limit output from the simplex method.
        model.setOut(null);
        //Solve the model.
        model.solve();
    }    

    
    //Get the optimal objective value
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

    //I create the constant and the linear term I need to create a feasibility cut in the master problem
    //The constant term is the demand and round up and down parameters multiplied with it's duals
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t = 1; t <= ucp.getnPeriods(); t++){
            constant += model.getDual(demandConstraints[t-1]) * ucp.getDemand(t);
        }
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                constant += model.getDual(RampUpConstraints[g-1][t-1]) * ucp.getRampUpLim(g) + model.getDual(RampDownConstraints[g-1][t-1]) * ucp.getRampDownLim(g);
            }
        }
        return constant;
    }
    
    //The linear term is the maxsimum and minimum power outputs multiplied with its duals and the optimal u-values from the previous master problem.
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){               
                cutTerm.addTerm(u[g-1][t-1], ucp.getMinOutput(g)*model.getDual(LowOutputConstraints[g-1][t-1]));
                cutTerm.addTerm(u[g-1][t-1], ucp.getMaxOutput(g)*model.getDual(HighOutputConstraints[g-1][t-1]));
            }
        }         
        return cutTerm;
    }
        
    
    //Get the optimal solution for L and P.
    public double[] getOptimalL() throws IloException{
        double L[] = new double[ucp.getnPeriods()];
        for(int t=1;t<=ucp.getnPeriods();t++){
            L[t-1] = model.getValue(l[t-1]);
        }
        return L;
    }
    
   public double[][] getOptimalP() throws IloException{
       double P[][] = new double[ucp.getnGenerators()][ucp.getnPeriods()];
       for(int g=1;g<=ucp.getnGenerators();g++){
           for(int t=1;t<=ucp.getnPeriods();t++){
               P[g-1][t-1] = model.getValue(p[g-1][t-1]);
           }
       }
       return P;
   }
   
   
   //Release all the resources
   public void end(){
       model.end();
   }

    
    
    
  
    
    
}
