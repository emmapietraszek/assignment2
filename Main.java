/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitCommitment;

import ilog.concert.IloException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Emma
 */
public class Main {
    public static void main(String[] arg) throws IloException, FileNotFoundException{
        
        //In the two files I can see how many generators and time periods there are.
        int nGenerators = 31;
        int nPeriods = 24; //Time periods is in this case hours.
        
        //First I read the file "loads.txt" and create a scanner for it.
        File f = new File("loads.txt");
        Scanner s = new Scanner(f);
        
        //I create a new double to contain the demand in each time period.
        double demands[] = new double[nPeriods];
        //For each time period I assign the next double in the file as the demand.
        for(int t=1;t<=nPeriods;t++){
            demands[t-1] = s.nextDouble();
        }
        
        //Then I read the "generators.txt" to get all of parameters for the generators and create a scanner for the file
        File h = new File("generators3.txt");
        Scanner c = new Scanner(h);
        
        //I create doubles for all the parameters, and a string for the generator names.
        String generatorNames[] = new String[nGenerators];
        double minOutputs[] = new double[nGenerators];
        double maxOutputs[] = new double[nGenerators];
        double startupCosts[] = new double[nGenerators];
        double commitmentCosts[] = new double[nGenerators];
        double rampUpLims[] = new double[nGenerators];
        double minOnTimes[] = new double[nGenerators];
        double minOffTimes[] = new double[nGenerators];        
        double marginalCosts[] = new double[nGenerators];
        
        /*I assign the first expression in the file to be the generator name. The following numbers is the 
        vulues for the different parameters. In order to read the file correctly I have put the doubles in exact order of the file. */
        for(int g=1;g<=nGenerators;g++){
        
            generatorNames[g-1] = c.next();
            minOutputs[g-1] = c.nextDouble();
            maxOutputs[g-1] = c.nextDouble();
            startupCosts[g-1] = c.nextDouble();
            commitmentCosts[g-1] = c.nextDouble();
            rampUpLims[g-1] = c.nextDouble();
            minOnTimes[g-1] = c.nextDouble();
            minOffTimes[g-1] = c.nextDouble();
            marginalCosts[g-1] = c.nextDouble();
            
        }
      
        
        
        //I create a new double for the load shredding costs
        double loadSheddingCosts[] = new double[nPeriods];
        //The load shredding costs is 46 for all time periods.
        for(int t=1;t<=nPeriods;t++){
            loadSheddingCosts[t-1] = 46;
        }
      
        //I create a new double for the ramp down limits
        double rampDownLims[] = new double[nGenerators];
        /*The ramp down limits is in this case the same as the camp up limits, since only one ramp
        parameter was given in the generators.txt */
        for(int g=1;g<=nGenerators;g++){
            rampDownLims[g-1] = rampUpLims[g-1];
        }
        
        //I create an instance of the problem with the data I have just read from the files.
        UnitCommitmentProblem ucp = new UnitCommitmentProblem(generatorNames,nPeriods,nGenerators,commitmentCosts,marginalCosts,startupCosts,loadSheddingCosts,
                minOnTimes,minOffTimes,minOutputs,maxOutputs,rampUpLims,rampDownLims,demands);
        
        //From this instance I create the master problem
        MasterProblem mp = new MasterProblem(ucp);
        
        //I solve the problem with Benders-Decomposition
        mp.solve();
        
        //I print the optimal objective value I get from Benders decomposition.
        System.out.println("Benders optimal objective = "+mp.getObjective());
        
        /*To be sure that I get the right result I solve the full model, so that I can compare the two.
        First I create the unit commitment model from the data, and then I solve it.
        The solve method also prints the optimal objective value*/
        UnitCommitmentModel ucm = new UnitCommitmentModel(ucp);
        ucm.solve();
        
        //If the objective values are the same my implementation of Benders is successful.
        
        
    }

    
}
