/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UnitCommitment;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 *
 * @author Emma
 */
public class FeasibilitySubProblem {
    
    //In this class I create constructors for the feasibility sub problem and the terms in the feasibility cuts. 

    private final IloCplex model;
    
    //My desition varibales are p and l.
    private final IloNumVar p[][];
    private final IloNumVar l[];
    
    //I need a slack variable for each of the constraints in the FSP, so I create five slack varibles.
    private final IloNumVar w1[];
    private final IloNumVar w2[][];
    private final IloNumVar w3[][];
    private final IloNumVar w4[][];
    private final IloNumVar w5[][];
    
    private final UnitCommitmentProblem ucp;
    
    //I also create objects to contain the constraints in the FSP.
    private final IloRange demandConstraints[];
    private final IloRange LowOutputConstraints[][];
    private final IloRange HighOutputConstraints[][];
    private final IloRange RampUpConstraints[][];
    private final IloRange RampDownConstraints[][];
    
    
    public FeasibilitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException{
        //I set the unit commitment problem
        this.ucp = ucp;
        
        //I create an IloCplex object for the model
        this.model = new IloCplex();
        
        /*I assign the p-varibale dimensions and a IloNumVar in each entrance.
        I do not define p at time 0, but instead make an "if"-statement in the constraints where I need to.*/
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        //I do the same for the l-variables
        l = new IloNumVar[ucp.getnPeriods()];
        for(int t=1;t<=ucp.getnPeriods();t++){
            l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        //I also give all the slack varibales dimentions and initialize them
        w1 = new IloNumVar[ucp.getnPeriods()];
        w2 = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        w3 = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        w4 = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        w5 = new IloNumVar[ucp.getnGenerators()][ucp.getnPeriods()];
        
        for(int t=1;t<=ucp.getnPeriods();t++){
            w1[t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w1_"+t);
        }
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
            w2[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w2_"+g+"_"+t);    
            w3[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w3_"+g+"_"+t);
            w4[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w4_"+g+"_"+t);
            w5[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w5_"+g+"_"+t);
            }
        }

        //I create an object for the objective function
        IloLinearNumExpr obj = model.linearNumExpr();
        
        //The objecgive function is the sum af all the slack variables (w's)
        for(int t=1;t<=ucp.getnPeriods();t++){
            obj.addTerm(w1[t-1], 1);
        }
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                obj.addTerm(w2[g-1][t-1], 1);
                obj.addTerm(w3[g-1][t-1], 1);
                obj.addTerm(w4[g-1][t-1], 1);
                obj.addTerm(w5[g-1][t-1], 1);
            }
        }
        //Tell the model to minimize the objective function
        model.addMinimize(obj);
        
        
        /*Demand constraints (1e)
        First I create the consiner of the constraints as an Ilo range.
        Then I add each constraint of this type to the range.
        Notice that I only subtract a slack varible since the l-varibale works as positive slack.*/
        this.demandConstraints = new IloRange[ucp.getnPeriods()];
        for(int t=1;t<=ucp.getnPeriods();t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g=1;g<=ucp.getnGenerators();g++){
                lhs.addTerm(p[g-1][t-1], 1);                
            }
            lhs.addTerm(l[t-1], 1);
            lhs.addTerm(w1[t-1], -1);
            demandConstraints[t-1] = model.addEq(lhs,ucp.getDemand(t));
        }
        
        //Lower bounds on power outputs for the (1f)
        this.LowOutputConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(p[g-1][t-1], 1);
                lhs.addTerm(w2[g-1][t-1], 1);
                LowOutputConstraints[g-1][t-1] = model.addGe(lhs, ucp.getMinOutput(g)*U[g-1][t-1]);
            }
        }
        
        //Upper bounds on power outputs for the generators (1g)
        this.HighOutputConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(p[g-1][t-1], 1);
                lhs.addTerm(w3[g-1][t-1], -1);
                HighOutputConstraints[g-1][t-1] = model.addLe(lhs, ucp.getMaxOutput(g)*U[g-1][t-1]);
            }
        }
        
        //Ramp-up conditions (1h)
        this.RampUpConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(p[g-1][t-1], 1);
                lhs.addTerm(w4[g-1][t-1], -1);
                //Since the generators are not procucing any power at t=0, I do not subtract anyting further when t=1,
                //because it reffers to the entrance -1 in the array which does not exist.
                if(t > 1){
                    lhs.addTerm(p[g-1][t-2],-1);
                }
                RampUpConstraints[g-1][t-1] = model.addLe(lhs, ucp.getRampUpLim(g));
            }
        }
        
        //Ramp-down conditions (1i)
        this.RampDownConstraints = new IloRange[ucp.getnGenerators()][ucp.getnPeriods()];
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                //Since p_g,0=0 I only add the term when t>1.
                if(t > 1){
                lhs.addTerm(p[g-1][t-2],1);
                }
                lhs.addTerm(p[g-1][t-1], -1);
                lhs.addTerm(w5[g-1][t-1], -1);
                RampDownConstraints[g-1][t-1] = model.addLe(lhs, ucp.getRampDownLim(g));
            }
        }
 
        
        
    }
    
    //Solve the problem
    public void solve() throws IloException{
        model.setOut(null);
        //This let's me avoid gettting too much information from the simplex method.
        
        //Solve the model.
        model.solve();
    }
    
    //Get the optimal objective value
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    
    /*I create the constant and the linear term I for a feasibility cut in the master problem.
    The constant term is the demand and round up and down parameters multiplied with it's duals.*/
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t=1;t<=ucp.getnPeriods();t++){
            constant += model.getDual(demandConstraints[t-1]) * ucp.getDemand(t);
            for(int g=1;g<=ucp.getnGenerators();g++){
                constant += model.getDual(RampUpConstraints[g-1][t-1]) * ucp.getRampUpLim(g)+model.getDual(RampDownConstraints[g-1][t-1]) * ucp.getRampDownLim(g);
            }
        }
        return constant;
    }
    
    //The linear term is the maxsimum and minimum power outputs multiplied with its duals and the optimal u-values from the previous master problem.
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g=1;g<=ucp.getnGenerators();g++){
            for(int t=1;t<=ucp.getnPeriods();t++){               
                cutTerm.addTerm(u[g-1][t-1], ucp.getMinOutput(g)*model.getDual(LowOutputConstraints[g-1][t-1]));
                cutTerm.addTerm(u[g-1][t-1], ucp.getMaxOutput(g)*model.getDual(HighOutputConstraints[g-1][t-1]));
            }
        }         
        return cutTerm;
    }
    
    
    
    
    
    
    //Release all objects
    public void end(){
        model.end();
    }
    
    

}
